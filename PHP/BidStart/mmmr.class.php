<?php
class mmmr {
	private $_numbers;
	
	public function __construct($numbers){
		if(!isset($numbers))
			throw new InvalidArgumentException("Argument must be a numeric array of some length.");
		if(!is_array($numbers) || empty($numbers))
			throw new InvalidArgumentException("Argument must be a numeric array of some length.");
		
		array_walk($numbers, "is_numeric_array");
		$this->_numbers = $numbers;
	}
	
	public function ProcessMMMR(){
		$results = array(
			"mean" => $this->mean(),
			"median" => $this->median(),
			"mode" => $this->mode(),
			"range" => $this->calc_range()
		);
		
		return $results;
	}
	
	public function mean(){
		$num_items = (float) count($this->_numbers);
		$sum = (float) array_sum($this->_numbers);
		return round(($sum / $num_items), 3);
	}
	
	public function median(){
		$temp_numbers = $this->_numbers;
		$mid = round(count($temp_numbers) / 2);
		sort($temp_numbers);
		if(count($temp_numbers) % 2 == 0)
			return round(($temp_numbers[$mid-1] + $temp_numbers[$mid]) / 2, 3);
		else
			return $temp_numbers[$mid-1];
	}
	
	public function mode(){
		$temp_numbers = $this->_numbers;
		array_walk($temp_numbers, 'mode_prep');
		$mode_array = array_count_values($temp_numbers);
		arsort($mode_array);
		$cur_mode = null;
		foreach($mode_array AS $mode => $value){
			if($value == 1)
				break;

			if(is_null($cur_mode)){
				$cur_mode = array(
					"mode" => $mode,
					"value" => $value
				);
				continue;
			}
			
			if($cur_mode["value"] == $value){
				if(!is_array($cur_mode["mode"]))
					$cur_mode["mode"] = array(
						$cur_mode["mode"],
						$mode
					);
				else
					$cur_mode["mode"][] = $mode;
				sort($cur_mode["mode"]);
				continue;
			}
			
			if($cur_mode["value"] > $value)
				break;				
		}
		
		return $cur_mode["mode"];
	}
	
	public function calc_range(){
		$temp_numbers = $this->_numbers;
		sort($temp_numbers);
		$min = (float) $temp_numbers[0];
		rsort($temp_numbers);
		$max = (float) $temp_numbers[0];
		return round(($max - $min), 3);
	}
}

//helper functions
function is_numeric_array($value, $key){
	if(!is_numeric($value))
		throw new InvalidArgumentException("All values must be numeric");
}

/**
 * mode_prep
 * To save some processing time, I use the built-in
 * array_count_values function. This requires the values of the array
 * to be integers or strings, so we need to prep the numeric array.
 */
function mode_prep(&$item1, $key){
	$item1 = "$item1";
}
?>
<?php
	if(isset($_GET))
		exit(json_encode(array(
			"error" => array(
				"code" => 404,
				"message" => "Method GET not available on this
					endpoint"
			)
		)));
	
	
	require_once("mmmr.class.php");

	$results = array();
	try{
		$JPOST = json_decode(file_get_contents('php://input'), true);
		$mmmr = new mmmr($JPOST["numbers"]);
		$results = $mmmr->ProcessMMMR();
		
		foreach($results AS &$value){
			if(is_null($value))
				$value = "";
		}
		
		$results = array(
			"results" => $results
		);
	} catch(Exception $e){
		$results = array(
			"error" => array(
				"code" => 500,
				"message" => $e->getMessage()
			)
		);
	}
	
	exit(json_encode($results));
?>
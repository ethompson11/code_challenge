<?php
require_once("mmmr.class.php");
class MMMRTest extends PHPUnit_Framework_TestCase {
		
	/**
	 * @dataProvider providerTestMMMR
	 */
	public function testMMMR($numbers){
		$this->assertInstanceOf('mmmr', new mmmr($numbers));
	}
	
	/**
	 * @dataProvider providerTestMMMRNonArrayException
	 * @expectedException InvalidArgumentException
	 * @expectedExceptionMessage Argument must be a numeric array of some length.
	 */
	public function testMMMRNonArrayException($non_array){
		$mmmr = new mmmr($non_array);
	}
	
	/**
	 * @dataProvider providerTestMMMRNonNumericException
	 * @expectedException InvalidArgumentException
	 * @expectedExceptionMessage All values must be numeric
	 */
	public function testMMMRNonNumericException($non_numeric){
		$mmmr = new mmmr($non_numeric);
	}
	
	/**
	 * @dataProvider providerTestMean
	 */
	public function testMean($numbers, $expected){
		$mmmr = new mmmr($numbers);
		
		$this->assertEquals($expected, $mmmr->mean());
	}
	
	/**
	 * @dataProvider providerTestMedian
	 */
	public function testMedian($numbers, $expected){
		$mmmr = new mmmr($numbers);
		
		$this->assertEquals($expected, $mmmr->median());
	}
	
	/**
	 * @dataProvider providerTestMode
	 */
	public function testMode($numbers, $expected){
		$mmmr = new mmmr($numbers);
		
		$this->assertEquals($expected, $mmmr->mode());
	}
	
	/**
	 * @dataProvider providerTestCalcRange
	 */
	public function testCalcRange($numbers, $expected){
		$mmmr = new mmmr($numbers);
		
		$this->assertEquals($expected, $mmmr->calc_range());
	}
	
	/**
	 * @dataProvider providerTestProcessMMMR
	 */
	public function testProcessMMMR($numbers, $expected){
		$mmmr = new mmmr($numbers);
		
		$this->assertEquals($expected, $mmmr->ProcessMMMR());
	}
	
	public function providerTestMMMR(){
		return array(
			array(array('1.2', 2.3, 3.4, 4.5, 5.6)),
			array(array(1.3, "2.4", 3.5, 4.6, 5.7)),
			array(array(1235308.1234, 1221984.54621, 49813089498.5498432, 365498132198.8448123)),
			array(array(1,2,3,4,5,6.986324)),
		);
	}
	
	public function providerTestMMMRNonArrayException(){
		return array(
			array(null),
			array(array()),
			array(1.1),
		);
	}
	
	public function providerTestMMMRNonNumericException(){
		return array(
			array(array('A', 2.3, 3.4, 4.5, 5.6)),
			array(array(1.3, 'B', 3.5, 4.6, 5.7)),
			array(array(1235308.1234, 1221984.54621, 'C', 365498132198.8448123)),
			array(array(1,2,3,'D',5,6.986324)),
		);
	}
	
	public function providerTestMean(){
		return array(
			array(array('1.2', 2.3, 3.4, 4.5, 5.6), 3.4),
			array(array(1.3, "2.4", 3.5, 4.6, 5.7), 3.5),
			array(array(1235308.1234, 1221984.54621, 49813089498.5498432, 365498132198.8448123), 103828419747.516),
			array(array(1,2,3,4,5,6.986324), 3.664),
		);
	}
	
	public function providerTestMedian(){
		return array(
			array(array('1.2', 2.3, 3.4, 4.5, 5.6), 3.4),
			array(array(1.3, "2.4", 3.5, 4.6, 5.7), 3.5),
			array(array(1235308.1234, 1221984.54621, 49813089498.5498432, 365498132198.8448123), 24907162403.337),
			array(array(1,2,3,4,5,6.986324), 3.5),
		);
	}
	
	public function providerTestMode(){
		return array(
			array(array(1,2,2,3,4,5,5,5,7,8,2), array(2,5)),
			array(array(1,2,2,3,4,5,5,5,7,8,2,5), 5),
			array(array(1,2,3,4), null),
		);
	}
	
	public function providerTestCalcRange(){
		return array(
			array(array('1.2', 2.3, 3.4, 4.5, 5.6), 4.4),
			array(array(1.3, "2.4", 3.5, 4.6, 5.7), 4.4),
			array(array(1235308.1234, 1221984.54621, 49813089498.5498432, 365498132198.8448123), 365496910214.299),
			array(array(1,2,3,4,5,6.986324), 5.986),
		);
	}
	
	public function providerTestProcessMMMR(){
		return array(
			array(
				array('1.2', 2.3, 3.4, 4.5, 5.6), 
				array(
					"mean" => 3.4,
					"median" => 3.4,
					"mode" => null,
					"range" => 4.4,
				)
			),
			array(
				array(1,2,2,3,4,5,5,5,7,8,2), 
				array(
					"mean" => 4.0,
					"median" => 4,
					"mode" => array(2,5),
					"range" => 7.0,
				)
			),
			array(
				array(1,2,2,3,4,5,5,5,7,8,2,5), 
				array(
					"mean" => 4.083,
					"median" => 4.5,
					"mode" => 5,
					"range" => 7.0,
				)
			),
			array(
				array(1.3, "2.4", 3.5, 4.6, 5.7), 
				array(
					"mean" => 3.5,
					"median" => 3.5,
					"mode" => null,
					"range" => 4.4,
				)
			),
			array(
				array(1235308.1234, 1221984.54621, 49813089498.5498432, 365498132198.8448123), 
				array(
					"mean" => 103828419747.516,
					"median" => 24907162403.337,
					"mode" => null,
					"range" => 365496910214.299,
				)
			),
			array(
				array(1,2,3,4,5,6.986324), 
				array(
					"mean" => 3.664,
					"median" => 3.5,
					"mode" => null,
					"range" => 5.986,
				)
			),
		);
	}
}
?>
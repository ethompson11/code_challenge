BidStart PHP Code Challenge
Submitter: Edwin D. Thompson
Recruiter: Ashley Marshall, Kelaca
Version: 1.0
github repo: https://github.com/ethompson11/Code_Challenge.git
BitBucket: https://ethompson11@bitbucket.org/ethompson11/code_challenge.git

Goal 1: Create a PHP library that calculates mean, median, mode and range of a set of numbers.

Goal 2: Make this library available via an API that returns a JSON object.

Installation: Unzip the contents into the desired location on the webserver and access accordingly.
